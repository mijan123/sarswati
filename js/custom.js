 
jQuery(document).ready(function( $ ){
 
 
       //------- Wow JS Initialized --------// 
    new WOW().init();

// ---fancybox
$(document).ready(function() {
  $("a.fancybox").fancybox()
});


   $(window).on("scroll", function() {
    if($(window).scrollTop() > 50) {
        $(".header__content-wrap").addClass("fixed");
        $(".header__content-wrap").css("background-color","##32a852");
        $(".header__content-wrap").css("top","0");
        $(".site-logo").css("padding","15px 0px");
        
        
       
       
    } else {
        $(".header__content-wrap").removeClass("fixed");
         $(".header__content-wrap").css("top","20%;");
         $(".site-logo").css("padding","25px 0px");
         
        
    }

});
   $.each($(".menu-item-has-children-first"), function(i,d){
    $(d).append(`
        <button class="dropdown-toggle dropdown-toggle-first">
            <i class="fa fa-chevron-down" aria-hidden="true"></i>
        </button>
    `)
})
      $.each($(".menu-item-has-children-second"), function(i,d){
    $(d).append(`
        <button class="dropdown-toggle dropdown-toggle-second">
            <i class="fa fa-chevron-down" aria-hidden="true"></i>
        </button>
    `)
})
   // responsive button
    $(".menu-mobile button.mobile-button").click(function (e) {
    e.preventDefault();
    
    $(".menu-mobile-holder").toggleClass("active");
  });
   $(".menu-mobile-holder ul li .dropdown-toggle-first").click(function (e) {
    e.preventDefault();
    
    $(".sub-menu-tour").toggleClass("show");
  });
     $(".menu-mobile-holder ul li .dropdown-toggle-second").click(function (e) {
    e.preventDefault();
    
    $(".sub-menu-about").toggleClass("show");
  });
  //   $(".menu-mobile-holder ul li .dropdown-toggle").click(function (e) {
  //   e.preventDefault();
    
  //   $(".sub-menu").removeClass("show");
  // });

  var $grid = $('.grid').isotope({
    itemSelector: '.element-item',
    layoutMode: 'fitRows',
     filter: '.subject1'
  });
  // filter functions
  var filterFns = {
    // show if number is greater than 50
  
    // show if name ends with -ium
  
  };
  // bind filter button click
  $('.filters-button-group').on( 'click', 'button', function() {
    var filterValue = $( this ).attr('data-filter');
    // use filterFn if matches value
    filterValue = filterFns[ filterValue ] || filterValue;
    $grid.isotope({ filter: filterValue });
  });
  // change is-checked class on buttons
  $('.button-group').each( function( i, buttonGroup ) {
    var $buttonGroup = $( buttonGroup );
    $buttonGroup.on( 'click', 'button', function() {
      $buttonGroup.find('.is-checked').removeClass('is-checked');
      $( this ).addClass('is-checked');
    });
  });
  
// slick
$(".sc-banner-slider").slick({
    autoplay: true,
    dots: false,
    arrows:  false ,
    infinite: true,
    fade:true,
    slidesToShow: 1,
    slidesToScroll: 1, 
     responsive: [
    {
      breakpoint: 1024,
      settings: {
        dots:true,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
   
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        vertical:false


      }
    },
    {
      breakpoint: 480,
      settings: {
        autoplay:false,
        vertical:false,
       
        arrows:false,
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
$(".notice-slider").slick({
    autoplay: true,
    dots: false,
    arrows: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1, 
     responsive: [
    {
      breakpoint: 1024,
      settings: {
        dots:true,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
   
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        vertical:false


      }
    },
    {
      breakpoint: 480,
      settings: {
        autoplay:false,
        vertical:false,
       
        arrows:false,
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
$(".calender-month-slider").slick({
  dots: false,
  arrows: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  asNavFor:".calender-event-slider",
   responsive: [
  {
    breakpoint: 1024,
    settings: {
      dots:true,
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
 
    }
  },
  {
    breakpoint: 991,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1,
      vertical:false


    }
  },
  {
    breakpoint: 480,
    settings: {
      autoplay:false,
      vertical:false,
      
      arrows:true,
      slidesToShow: 1,
      slidesToScroll: 1
    }
  }
]
});
$(".calender-event-slider").slick({

  dots: false,
  arrows: false,
  fade:true,
  slidesToShow: 1,
  slidesToScroll: 1,
  asNavFor:".calender-month-slider",
   responsive: [
  {
    breakpoint: 1024,
    settings: {
      dots:false,
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
 
    }
  },
  {
    breakpoint: 991,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1,
      vertical:false


    }
  },
  {
    breakpoint: 480,
    settings: {
      autoplay:false,
      vertical:false,
      centerMode: true,
      arrows:false,
      slidesToShow: 1,
      slidesToScroll: 1
    }
  }
]
});
$(".testimonial-slider").slick({
  autoplay: true,
  dots: false,
  arrows: true,
  infinite: true,
  
  slidesToShow: 1,
  slidesToScroll: 1,
   responsive: [
  {
    breakpoint: 1024,
    settings: {
      dots:true,
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
 
    }
  },
  {
    breakpoint: 991,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1,
      vertical:false


    }
  },
  {
    breakpoint: 480,
    settings: {
      autoplay:false,
      vertical:false,
      
      arrows:true,
      slidesToShow: 1,
      slidesToScroll: 1
    }
  }
]
});
   });